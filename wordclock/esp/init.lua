--connect to Access Point (DO save config to flash)
station_cfg={}
station_cfg.ssid="bridge"
station_cfg.pwd="freshcheese419"
station_cfg.save=true
wifi.sta.config(station_cfg)

dofile("time.lua")
dofile("wordclock.lua")

local booting_light_phase = 0

function booting_light()
    booting_light_phase = (booting_light_phase +1)%5
    word_write(light({}, dots[booting_light_phase]))
end

booting_timer = tmr.create()
booting_timer.alarm(booting_timer, 500, tmr.ALARM_AUTO, booting_light)

function update_time()
    time = get_zoned_time()
    word_write(hour_leds(time["hour"], time["min"]))
end

function callback()
    booting_timer:stop()
    update_time()
    timer = tmr.create()
    timer.alarm(timer, 1000*10, tmr.ALARM_AUTO, update_time)
end


init_ntp(callback)
