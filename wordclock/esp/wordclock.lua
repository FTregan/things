
--connect to Access Point (DO save config to flash)
--station_cfg={}
--station_cfg.ssid="bridge"
--station_cfg.pwd="freshcheese419"
--station_cfg.save=true
--wifi.sta.config(station_cfg)

on = string.char(20, 0, 200, 200)
off = string.char(10, 20, 0, 0)
 
leds =  {
    ["H_1"] = {1, 2},
    ["H_2"] = {3, 4, 5},
    ["H_3"] = {14, 15, 16},
    ["H_4"] = {17, 18, 19, 20},
    ["H_5"] = {30, 31, 32},
    ["H_6"] = {21, 22},
    ["H_7"] = {33, 34},
    ["H_8"] = {12, 13},
    ["H_9"] = {27, 28, 29},
    ["H_10"] = {23, 24},
    ["H_11"] = {6, 7, 8},
    ["midi"] = {25, 26},
    ["minuit"] = {9, 10, 11},
    ["heure"] = {35, 36, 37},
    ["s"] = {38},
    ["M_5"] = {49, 50, 51},
    ["M_10"] = {47, 48},
    ["quart"] = {62, 63, 64},
    ["M_20"] = {59, 60, 61},
    ["M_25"] = {52, 53, 54, 55, 56},
    ["demie"] = {44, 45, 46},
    ["le"] = {57, 58},
    ["et"] = {39, 40},
    ["moins"] = {41, 42, 43},
    ["D_1"] = {65},
    ["D_2"] = {66},
    ["D_3"] = {67},
    ["D_4"] = {68}
}


hours= {
        [0] =  {leds["minuit"]},
        [1] =  {leds["H_1"],  leds["heure"]},
        [2] =  {leds["H_2"],  leds["heure"], leds["s"]},
        [3] =  {leds["H_3"],  leds["heure"], leds["s"]},
        [4] =  {leds["H_4"],  leds["heure"], leds["s"]},
        [5] =  {leds["H_5"],  leds["heure"], leds["s"]},
        [6] =  {leds["H_6"],  leds["heure"], leds["s"]},
        [7] =  {leds["H_7"],  leds["heure"], leds["s"]},
        [8] =  {leds["H_8"],  leds["heure"], leds["s"]},
        [9] =  {leds["H_9"],  leds["heure"], leds["s"]},
        [10] = {leds["H_10"], leds["heure"], leds["s"]},
        [11] = {leds["H_11"], leds["heure"], leds["s"]},
        [12] = {leds["midi"]},
        [13] = {leds["H_1"],  leds["heure"], leds["s"]},
        [14] = {leds["H_2"],  leds["heure"], leds["s"]},
        [15] = {leds["H_3"],  leds["heure"], leds["s"]},
        [16] = {leds["H_4"],  leds["heure"], leds["s"]},
        [17] = {leds["H_5"],  leds["heure"], leds["s"]},
        [18] = {leds["H_6"],  leds["heure"], leds["s"]},
        [19] = {leds["H_7"],  leds["heure"], leds["s"]},
        [20] = {leds["H_8"],  leds["heure"], leds["s"]},
        [21] = {leds["H_9"],  leds["heure"], leds["s"]},
        [22] = {leds["H_10"], leds["heure"], leds["s"]},
        [23] = {leds["H_11"], leds["heure"], leds["s"]}
}

minutes = {
    [0] = {},
    [5] = {leds["M_5"]},
    [10] = {leds["M_10"]},
    [15] = {leds["et"], leds["quart"]},
    [20] = {leds["M_20"]},
    [25] = {leds["M_25"]},
    [30] = {leds["et"], leds["demie"]},
    [35] = {leds["moins"], leds["M_25"]},
    [40] = {leds["moins"], leds["M_20"]},
    [45] = {leds["moins"], leds["le"], leds["quart"]},
    [50] = {leds["moins"], leds["M_10"]},
    [55] = {leds["moins"], leds["M_5"]}
}

dots = {
    [0] = {{}},
    [1] = {leds["D_1"]},
    [2] = {leds["D_1"], leds["D_2"]},
    [3] = {leds["D_1"], leds["D_2"], leds["D_3"]},
    [4] = {leds["D_1"], leds["D_2"], leds["D_3"], leds["D_4"]}
}

function light(led_status, leds_to_light)
    for i, case in ipairs(leds_to_light) do
        for j, led in ipairs(case) do
            led_status[led]=true
        end
    end

    return led_status
end

function hour_leds(hour, min)
    rounded_minute = min - min%5

    led_status={}

    my_hour = hour
    if (rounded_minute > 30) then
        my_hour = (hour+1)%24
    end
    
    light(led_status, hours[my_hour])
    
    light(led_status, minutes[rounded_minute])

    light(led_status, dots[min%5])

    return led_status
end

function word_write(led_status)
    st = ""
    for i=1,68 do
        if led_status[i] then
            st=st..on
        else
            st=st..off
        end
    end
    apa102.write(2, 1, st)
end
