local retry = 0

function init_ntp(success)
    net.dns.resolve("0.fr.pool.ntp.org", 
        function(sk, ip)
            restart = function () init_ntp(success) end
            if (ip == nil) 
            then 
                print("DNS fail!, will retry in 1s") 
                timer = tmr.create()
                timer.alarm(timer, 1000, tmr.ALARM_SINGLE, restart)
            else 
                print(ip) 
                sntp.sync(ip, 
                    function(sec,usec,server)
                        print('sync', sec, usec, server)
                        success()
                    end,
                    function()
                        print("sync failed, will retry in 1s")
                        timer = tmr.create()
                        timer.alarm(timer, 1000, tmr.ALARM_SINGLE, restart)
                    end)
            end
        end)

end

function get_zoned_time()
    timestamp = rtctime.get()
    return rtctime.epoch2cal(timestamp + 2 * 60 * 60)
end


