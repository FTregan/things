# Icosahedral lamp shade

That directory contains svg file to build [isocahedral](https://en.wikipedia.org/wiki/Regular_icosahedron)(20 faces) lamp shade with a [serpinsky triangle](https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle) pattern.

They are meant to be laser cut in sheets of 3mm of thickness.
For example I cut it 3mm birch plywood.

There are 2 versions of the shade:

* small: 
  * diameter: around 19cm.
  * you need to cut shade-small.1.svg 2 times and shade-small.2.svg 1 time.
* big: 
  * diameter: around 28cm.
  * you need to cut shade-big.1.svg 6 times and shade-big.2.svg 1 time.