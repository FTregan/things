#include "FastLED.h"

// How many leds in your strip?
#define NUM_LEDS 5

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 7


// Define the array of leds
CRGB leds[NUM_LEDS];

void setup() { 
	Serial.begin(9600);
	Serial.println("resetting");
	
  LEDS.addLeds<WS2812B, DATA_PIN>(leds, NUM_LEDS);
}

void fadeall() { for(int i = 0; i < NUM_LEDS; i++) { leds[i].nscale8(250); } }

void loop() { 

  for (int i = 0; i < NUM_LEDS; i++) {
	  leds[i] = CRGB::White;
  }
	FastLED.show();

 

}